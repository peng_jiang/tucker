#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>
#include "jacobi_eigenvalue.h"

#define NDIM 3
#define MAXITER 20

void print_dim(int *dim, int m)
{
  if(m==2) {
    printf("%d %d\n", dim[0], dim[1]);
  } else if(m==3) {
    printf("%d %d %d\n", dim[0], dim[1], dim[2]);
  }
}



int get_3dindex(int i, int j, int k, int *dim) 
{
  return i * dim[1]*dim[2] + j * dim[2] + k;
}

int get_2dindex(int i, int j, int *dim) 
{
  return i * dim[1] + j;
}

double norm(double *X, int *dim) 
{
  int i;
  double af = 0.0;
  for(i=0;i<dim[0]*dim[1]*dim[2];i++) {
    af += X[i] * X[i];
  }
  af = sqrt(af);
  return af;
}

void unfold(double *X, int *dim, int n, double *Y, int *newdim)
{
  int i, j, k, t = 0;
  int a,b;

  if(n==0) {
    for(i=0;i<dim[0];i++) {
      for(j=0;j<dim[1];j++) {
        for(k=0;k<dim[2];k++) {
          Y[t++] = X[get_3dindex(i,j,k,dim)];
        }
      }
    }  
    newdim[0] = dim[0];
    newdim[1] = dim[1]*dim[2];
  }

  if(n==1) {
    for(i=0;i<dim[1];i++) {
      for(j=0;j<dim[0];j++) {
        for(k=0;k<dim[2];k++) {
          Y[t++] = X[get_3dindex(j,i,k,dim)];
        }
      }
    }  
    newdim[0] = dim[1];
    newdim[1] = dim[0]*dim[2];
  }
  if(n==2) {
    for(i=0;i<dim[2];i++) {
      for(j=0;j<dim[0];j++) {
        for(k=0;k<dim[1];k++) {
          Y[t++] = X[get_3dindex(j,k,i,dim)];
        }
      }
    }  
    newdim[0] = dim[2];
    newdim[1] = dim[0]*dim[1];
  }
}

void transpose_2d(double *X, int *dim, double *Y, int *newdim)
{
  int i, j;
  newdim[0] = dim[1];
  newdim[1] = dim[0];
  for(i=0;i<newdim[0];i++) {
    for(j=0;j<newdim[1];j++) {
      Y[get_2dindex(i,j,newdim)] = X[get_2dindex(j,i,dim)];
    }
  }
}

void multiply(double *X, int *dim, double *Y, int *dim1, double *res, int *resdim)
{
  int i,j,k;
  resdim[0] = dim[0];
  resdim[1] = dim[0];
  for(i=0;i<dim[0];i++) {
    for(j=0;j<dim[0];j++) {
      double tmp = 0.0;
      for(k=0;k<dim[1];k++) {
        tmp += X[get_2dindex(i,k,dim)]*Y[get_2dindex(k,j,dim1)];
      }
      res[get_2dindex(i,j,resdim)] = tmp;
    }
  }
}

//FROM HERE
void eigh(double *X, int* dim, int rank, double *V)
{
  int i;
  double *nV = (double *) malloc(dim[0] * dim[0] * sizeof(double));
  double *nd = (double *) malloc(dim[0]*sizeof(double));
  int it_num, rot_num;
  jacobi_eigenvalue(dim[0], X, MAXITER, nV, nd, &it_num, &rot_num );
  for(i=0;i<dim[0];i++) {
    V[i] = nV[i];
  }
}

void nvecs(double *X, int* dim, int *olddim, int n, int* rank, double *U)
{
  int size = dim[0] * dim[1] * dim[2];
  double *Xn = (double *)malloc(size*sizeof(double));
  double *XnT = (double *)malloc(size*sizeof(double));
  int newdim[2];
  unfold(X, dim, n, Xn, newdim);
  //print_dim(newdim, 2);
  int newdim2[2];
  transpose_2d(Xn, newdim, XnT, newdim2);
  //print_dim(newdim2, 2);
  int newdim3[2];
  double *res = (double *)malloc(newdim[0]*newdim2[1]*sizeof(double));
  multiply(Xn, newdim, XnT, newdim2, res, newdim3);
  //print_dim(newdim3, 2);
  double *V;
  if(n==0){V=U;}
  if(n==1){V=U+rank[0]*olddim[0];}
  if(n==2){V=U+rank[0]*olddim[0]+rank[1]*olddim[1];}
  eigh(res,newdim3,rank[n],V);
  free(Xn);
  free(XnT);
  free(res);
}

void init(double *X, int *dim, int *rank, double *Uinit)
{ 
  int i,j,k, t=0;
  for(i=0;i<3;i++) {
    for(j=0;j<dim[i];j++) {
      for(k=0;k<rank[i];k++) {
        Uinit[t++] = 1.0 * rand() / RAND_MAX;
      }
    }
  }
}

void transpose_3d(double *X, int *dim, int *axes, double *Y, int *newdim)
{
  int i,j,k;
  newdim[0] = dim[axes[0]];
  newdim[1] = dim[axes[1]];
  newdim[2] = dim[axes[2]];
  //printf("transposing: ");
  //print_dim(newdim, 3);

  for(i=0;i<dim[0];i++) {
    for(j=0;j<dim[1];j++) {
      for(k=0;k<dim[2];k++) {
        int index[3];
        int new_index[3];
        index[0] = i; index[1] = j; index[2] = k;
        new_index[0] = index[axes[0]];
        new_index[1] = index[axes[1]];
        new_index[2] = index[axes[2]];
        Y[get_3dindex(new_index[0], new_index[1], new_index[2],newdim)] = X[get_3dindex(i,j,k,dim)];
      }
    }
  }
}

void ttm(double *X, int *dim, int *rank, double *U, int mode, double *Utilde, int *newdim)
{
  int order[3];
  double *Y = (double *)malloc(sizeof(double)*dim[0]*dim[1]*dim[2]);
  //printf("X dim: ");
  //print_dim(dim, 3);
  if(mode==0){order[0]=0;order[1]=1;order[2]=2;}
  if(mode==1){order[0]=1;order[1]=0;order[2]=2;}
  if(mode==2){order[0]=2;order[1]=0;order[2]=1;}
  int dim2[3];
  transpose_3d(X, dim, order, Y, dim2);

  //printf("X transpose dim: ");
  //print_dim(dim2, 3);

  int dim3[2];
  dim3[0] = dim[order[0]];
  dim3[1] = dim[order[1]]*dim[order[2]];
  double *V;
  if(mode==0){V=U;}
  if(mode==1){V=U+rank[0]*dim[0];}
  if(mode==2){V=U+rank[0]*dim[0]+rank[1]*dim[1];}
  int dimV[2];
  dimV[0] = rank[mode];
  dimV[1] = dim[mode];
  int dimnewT[2];
  dimnewT[0] = rank[mode];
  dimnewT[1] = dim[order[1]] * dim[order[2]];
  double *newT = Utilde;
  int i,j,k;
  for(i=0;i<dimnewT[0];i++) {
    for(j=0;j<dimnewT[1];j++) {
      double tmp = 0.0;
      for(k=0;k<dim[mode];k++) {
        tmp += V[get_2dindex(i,k,dimV)] * Y[get_2dindex(k,j,dim3)];
      }
      newT[get_2dindex(i,j,dimnewT)] = tmp;
    }
  }
  newdim[0] = dimnewT[0];
  newdim[1] = dim[order[1]];
  newdim[2] = dim[order[2]];
  //printf("Utilde: ");
  //print_dim(newdim, 3);
  free(Y);
}

double* tucker(double *X, int *dim, int *rank)
{
  double normX;
  int iter, i;

  normX = norm(X, dim);

  double *U = (double *)malloc(sizeof(double)*(rank[0]*dim[0]+rank[1]*dim[1]+rank[2]*dim[2]));
  init(X, dim, rank, U);
  double fit = 0.0;
  double fitold;
  double *Utilde0 = (double *)malloc(sizeof(double)*dim[0]*dim[1]*dim[2]);
  double *Utilde1 = (double *)malloc(sizeof(double)*dim[0]*dim[1]*dim[2]);
  double *Utilde2 = (double *)malloc(sizeof(double)*dim[0]*dim[1]*dim[2]);
  double *core = (double *)malloc(sizeof(double)*dim[0]*dim[1]*dim[2]);
  
  for(iter=0;iter<MAXITER;iter++) {

    printf("iter: %d\n", iter);
    struct timeval tv1, tv2;
    struct timezone tz1, tz2;
    gettimeofday(&tv1, &tz1);
    fitold = fit;

    int newdim0[3];
    int newdim1[3];
    int newdim2[3];
//    for(i=0;i<3;i++) {
//      printf("mode: %d\n", i);
//
//#pragma omp parallel sections num_threads(3)
    {
        ttm(X,dim,rank,U,2,Utilde2,newdim2);
        nvecs(Utilde2,newdim2,dim,0,rank,U);
//#pragma omp section
      {
        ttm(X,dim,rank,U,0,Utilde0,newdim0);
        nvecs(Utilde0,newdim0,dim,0,rank,U);
      }
//#pragma omp section 
      {
        ttm(X,dim,rank,U,1,Utilde1,newdim1);
        nvecs(Utilde1,newdim1,dim,0,rank,U);
      }
      
    }
    int dimcore[3];
    ttm(Utilde2,newdim2,rank,U,2,core, dimcore);

    double normcore = norm(core,dimcore);
    double normresidual = sqrt(normX*normX-normcore*normcore);

    fit = 1 - (normresidual / normX);
    gettimeofday(&tv2, &tz2);
    printf("time per iteration: %f\n", ((tv2.tv_sec - tv1.tv_sec)*1000000 + (tv2.tv_usec-tv1.tv_usec))/1000000.0);
  //  printf("%lf\n", fabs(fit-fitold));
    if(fabs(fit-fitold) < 0.1){break;}
  }
  free(Utilde0);
  free(Utilde1);
  free(Utilde2);
  free(core);
  return U;
}

int main() 
{

  int dim[3] = {128,128,128};
  double *X = (double *)malloc(sizeof(double)*dim[0]*dim[1]*dim[2]);
  for(int i=0;i<dim[0];i++) {
    for(int j=0;j<dim[1];j++) {
      for(int k=0;k<dim[2];k++) {
        X[get_3dindex(i,j,k,dim)] = rand() % 2027;
      }
    }
  }
  int rank[3] = {1,1,1};
  double *U = tucker(X, dim, rank);
  for(int i=0;i<rank[0]*dim[0];i++) {
    printf("%lf ", U[i]);
  }
  printf("\n");
  for(int i=rank[0]*dim[0];i<rank[0]*dim[0]+rank[1]*dim[1];i++) {
    printf("%lf ", U[i]);
  }
  printf("\n");
  for(int i=rank[0]*dim[0]+rank[1]*dim[1];i<rank[0]*dim[0]+rank[1]*dim[1]+rank[2]*dim[2];i++) {
    printf("%lf ", U[i]);
  }
  printf("\n");

  return 0;
}

